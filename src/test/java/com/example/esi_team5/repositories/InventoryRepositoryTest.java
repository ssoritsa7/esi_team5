package com.example.esi_team5.repositories;

import com.example.esi_team5.EsiTeam5Application;
import com.example.esi_team5.models.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = EsiTeam5Application.class)
@Sql(scripts="/plants-dataset.sql")
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InventoryRepositoryTest {
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepo;
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepo;
    @Autowired
    PlantReservationRepository plantReservationRepo;
    @Autowired
    InventoryRepository inventoryRepo;
    @Autowired
    MaintenanceTaskRepository maintenanceTaskRepo;

    @Test
    public void queryPlantCatalog() {
        assertThat(plantInventoryEntryRepo.count()).isEqualTo(19l);
    }

    @Test
    public void queryByName() {
        assertThat(plantInventoryEntryRepo.findByNameContaining("Mini").size()).isEqualTo(2);
        assertThat(plantInventoryEntryRepo.finderMethod("mini").size()).isEqualTo(2);
        assertThat(plantInventoryEntryRepo.finderMethodV2("mini").size()).isEqualTo(2);
    }

    @Test
    public void findAvailableTest() {
        PlantInventoryEntry entry = plantInventoryEntryRepo.findById(1l).orElse(null);
        PlantInventoryItem item = plantInventoryItemRepo.findOneByPlantInfo(entry);

        assertThat(inventoryRepo.findAvailablePlants(entry.getName().toLowerCase(), LocalDate.of(2017,2,20), LocalDate.of(2017,2,25)))
                .contains(entry);

        PlantReservation po = new PlantReservation();
        po.setId(1l);
        po.setPlant(item);
        po.setSchedule(BusinessPeriod.of(LocalDate.of(2017, 2, 20), LocalDate.of(2017, 2, 25)));
        plantReservationRepo.save(po);

        assertThat(inventoryRepo.findAvailablePlants(entry.getName().toLowerCase(), LocalDate.of(2017,2,20), LocalDate.of(2017,2,25)))
                .doesNotContain(entry);
    }

    @Test
    public void findAvailablePlantsCountTest() {
        List<Object[]> availablePlants = inventoryRepo.findAvailablePlantsCount("bulldozer", LocalDate.of(2019,3,1), LocalDate.of(2019,3,14));
        assertThat(availablePlants).size().isEqualTo(3);
        assertThat(availablePlants.get(0)[1]).isEqualTo(2l);
        assertThat(availablePlants.get(1)[1]).isEqualTo(1l);
    }

    @Test
    public void findAllPlantsNotScheduledForMaintenanceInTwelveMonthsTest(){

        List<Object[]> allNotMaintainedPlants = inventoryRepo.findAllPlantsNotScheduledForMaintenanceInTwelveMonths();
        assertThat(allNotMaintainedPlants.size()).isEqualTo(3);

        MaintenanceTask maintenanceTask = new MaintenanceTask();
        maintenanceTask.setDescription("Engine Failure");
        maintenanceTask.setTypeOfWork(TypeOfWork.PREVENTIVE);
        maintenanceTask.setPrice(new BigDecimal(3000));
        maintenanceTask.setPlantReservation(plantReservationRepo.findById(1L).orElse(null));
        maintenanceTask.setBusinessPeriod(BusinessPeriod.of(LocalDate.of(2019,3,1), LocalDate.of(2019,3,10)));
        maintenanceTaskRepo.saveAndFlush(maintenanceTask);

        assertThat(allNotMaintainedPlants.size()).isEqualTo(3);
    }

    @Test
    public void findAvailablePlantsPOReservationTest() {
        //Reservation intersection check
        PlantInventoryItem item1 = plantInventoryItemRepo.findById(10004l).orElse(null);

        PlantReservation po = new PlantReservation();
        po.setId(1l);
        po.setPlant(item1);
        po.setSchedule(BusinessPeriod.of(LocalDate.of(2019, 2, 20), LocalDate.of(2019, 3, 5)));
        plantReservationRepo.save(po);
        List<Object[]> availablePlants = inventoryRepo.findAvailablePlantsCount("bulldozer", LocalDate.of(2019,3,1), LocalDate.of(2019,3,14));
        assertThat(availablePlants).size().isEqualTo(2);
        assertThat(availablePlants.get(0)[1]).isEqualTo(2l);

        item1 = plantInventoryItemRepo.findById(10002l).orElse(null);
        po = new PlantReservation();
        po.setId(1l);
        po.setPlant(item1);
        po.setSchedule(BusinessPeriod.of(LocalDate.of(2019, 2, 20), LocalDate.of(2019, 3, 5)));
        plantReservationRepo.save(po);
        availablePlants = inventoryRepo.findAvailablePlantsCount("bulldozer", LocalDate.of(2019,3,1), LocalDate.of(2019,3,14));
        assertThat(availablePlants).size().isEqualTo(3);
        assertThat(availablePlants.get(0)[1]).isEqualTo(1l);
    }

    @Test
    public void findAvailablePlantsMaintenanceReservationTest() {
        assertThat(inventoryRepo.findAvailablePlantsCount("bulldozer", LocalDate.of(2019,4,1), LocalDate.of(2019,4,14)))
                .size().isEqualTo(3);

        PlantInventoryEntry entry = plantInventoryEntryRepo.findById(1004l).orElse(null);
        PlantInventoryItem item = new PlantInventoryItem();
        item.setId(1l);
        item.setPlantInfo(entry);
        item.setEquipmentCondition(EquipmentCondition.UNSERVICEABLEREPAIRABLE);
        item.setSerialNumber("BD05");

        plantInventoryItemRepo.save(item);

        List<Object[]> availablePlants = inventoryRepo.findAvailablePlantsCount("bulldozer", LocalDate.of(2019,4,1), LocalDate.of(2019,4,14));

        assertThat(availablePlants).size().isEqualTo(4);
    }

    @Test
    public void findAvailablePlantsMaintenanceIntersectionTest() {
        assertThat(inventoryRepo.findAvailablePlantsCount("bulldozer", LocalDate.of(2019,4,1), LocalDate.of(2019,4,14)))
                .size().isEqualTo(3);

        PlantInventoryEntry entry = plantInventoryEntryRepo.findById(1004l).orElse(null);
        PlantInventoryItem item = new PlantInventoryItem();
        item.setId(1l);
        item.setPlantInfo(entry);
        item.setEquipmentCondition(EquipmentCondition.UNSERVICEABLEREPAIRABLE);
        item.setSerialNumber("BD05");

        plantInventoryItemRepo.save(item);

        item = plantInventoryItemRepo.findById(1L).orElse(null);
        PlantReservation po = new PlantReservation();
        po.setId(1l);
        po.setPlant(item);
        po.setSchedule(BusinessPeriod.of(LocalDate.of(2019, 2, 20), LocalDate.of(2019, 3, 30)));
        plantReservationRepo.save(po);

        MaintenanceTask maintenanceTask = new MaintenanceTask();
        maintenanceTask.setDescription("Fix scratches all over");
        maintenanceTask.setTypeOfWork(TypeOfWork.CORRECTIVE);
        maintenanceTask.setPrice(new BigDecimal(4000));
        maintenanceTask.setPlantReservation(plantReservationRepo.findById(1L).orElse(null));
        maintenanceTask.setBusinessPeriod(BusinessPeriod.of(LocalDate.of(2019, 2, 20), LocalDate.of(2019, 3, 30)));
        maintenanceTaskRepo.saveAndFlush(maintenanceTask);

        List<Object[]> availablePlants = inventoryRepo.findAvailablePlantsCount("bulldozer", LocalDate.of(2019,4,1), LocalDate.of(2019,4,14));

        assertThat(availablePlants).size().isEqualTo(3);
    }

    @Test
    public void findPreventiveRepairsByFiveYearsTest(){

        List<Object[]> preventiveRepairsByYears = inventoryRepo.findPreventiveRepairsByYears(5);

        assertThat(preventiveRepairsByYears.size()).isEqualTo(5);

        assertThat(preventiveRepairsByYears.get(0)[0]).isEqualTo(2018);
        BigDecimal cost2018 = (BigDecimal) preventiveRepairsByYears.get(0)[1];
        assertThat(cost2018.compareTo(new BigDecimal(10))).isEqualTo(1);

        assertThat(preventiveRepairsByYears.get(1)[0]).isEqualTo(2017);
        BigDecimal cost2017 = (BigDecimal) preventiveRepairsByYears.get(1)[1];
        assertThat( cost2017.compareTo(new BigDecimal(20))).isEqualTo(0);

        MaintenanceTask maintenanceTask = new MaintenanceTask();
        maintenanceTask.setDescription("broken screw");
        maintenanceTask.setTypeOfWork(TypeOfWork.PREVENTIVE);
        maintenanceTask.setPrice(new BigDecimal(30));
        maintenanceTask.setPlantReservation(plantReservationRepo.findById(1L).orElse(null));
        maintenanceTask.setBusinessPeriod(BusinessPeriod.of(LocalDate.of(2017,2,2), LocalDate.of(2017,2,2)));
        maintenanceTaskRepo.saveAndFlush(maintenanceTask);

        List<Object[]> newPreventiveRepairsByYears = inventoryRepo.findPreventiveRepairsByYears(5);
        assertThat(newPreventiveRepairsByYears.get(1)[0]).isEqualTo(2017);
        BigDecimal cost2017New = (BigDecimal) newPreventiveRepairsByYears.get(1)[1];
        assertThat( cost2017New.compareTo(new BigDecimal(50))).isEqualTo(0);
    }

    @Test
    public void findPlantsInPeriodNewPlantReservationTest() {
        List<Object[]> plantsInPeriod = inventoryRepo.findPlantsInPeriod(LocalDate.of(2017,2,20), LocalDate.of(2017,2,25));

        assertThat(plantsInPeriod.size()).isEqualTo(3);
        assertThat(plantsInPeriod.get(0)[1]).isEqualTo(2L);
        assertThat(plantsInPeriod.get(0)[2]).isEqualTo(5L);

        PlantReservation plantReservation = new PlantReservation();
        plantReservation.setId(10L);
        plantReservation.setPlant(plantInventoryItemRepo.findById(10005L).orElse(null));
        plantReservation.setSchedule(BusinessPeriod.of(LocalDate.of(2017,2,18), LocalDate.of(2017,2,23)));
        plantReservationRepo.saveAndFlush(plantReservation);


        List<Object[]> updatedPlantsInPeriod = inventoryRepo.findPlantsInPeriod(LocalDate.of(2017,2,20), LocalDate.of(2017,2,25));

        assertThat(updatedPlantsInPeriod.size()).isEqualTo(4);
        assertThat(updatedPlantsInPeriod.get(3)[1]).isEqualTo(1L);
        assertThat(updatedPlantsInPeriod.get(3)[2]).isEqualTo(0L);
    }

    @Test
    public void findPlantsInPeriodNewMaintenanceTaskTest() {
        List<Object[]> plantsInPeriod = inventoryRepo.findPlantsInPeriod(LocalDate.of(2017,2,20), LocalDate.of(2017,2,25));

        assertThat(plantsInPeriod.size()).isEqualTo(3);
        assertThat(plantsInPeriod.get(0)[1]).isEqualTo(2L);
        assertThat(plantsInPeriod.get(0)[2]).isEqualTo(5L);

        MaintenanceTask maintenanceTask = new MaintenanceTask();
        maintenanceTask.setDescription("loose wheel");
        maintenanceTask.setTypeOfWork(TypeOfWork.CORRECTIVE);
        maintenanceTask.setPrice(new BigDecimal(30));
        maintenanceTask.setPlantReservation(plantReservationRepo.findById(2L).orElse(null));
        maintenanceTask.setBusinessPeriod(BusinessPeriod.of(LocalDate.of(2017,2,2), LocalDate.of(2017,2,2)));
        maintenanceTaskRepo.saveAndFlush(maintenanceTask);

        List<Object[]> updatedPlantsInPeriod = inventoryRepo.findPlantsInPeriod(LocalDate.of(2017,2,20), LocalDate.of(2017,2,25));

        assertThat(updatedPlantsInPeriod.size()).isEqualTo(3);
        assertThat(updatedPlantsInPeriod.get(0)[1]).isEqualTo(2L);
        assertThat(updatedPlantsInPeriod.get(0)[2]).isEqualTo(6L);
    }
}