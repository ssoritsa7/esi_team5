insert into plant_inventory_entry (id, name, description, price)
    values (1, 'Mini excavator', '1.5 Tonne Mini excavator', 150);
insert into plant_inventory_entry (id, name, description, price)
    values (2, 'Mini excavator', '3 Tonne Mini excavator', 200);
insert into plant_inventory_entry (id, name, description, price)
    values (3, 'Midi excavator', '5 Tonne Midi excavator', 250);
insert into plant_inventory_entry (id, name, description, price)
    values (4, 'Midi excavator', '8 Tonne Midi excavator', 300);
insert into plant_inventory_entry (id, name, description, price)
    values (5, 'Maxi excavator', '15 Tonne Large excavator', 400);
insert into plant_inventory_entry (id, name, description, price)
    values (6, 'Maxi excavator', '20 Tonne Large excavator', 450);
insert into plant_inventory_entry (id, name, description, price)
    values (7, 'HS dumper', '1.5 Tonne Hi-Swivel Dumper', 150);
insert into plant_inventory_entry (id, name, description, price)
    values (8, 'FT dumper', '2 Tonne Front Tip Dumper', 180);
insert into plant_inventory_entry (id, name, description, price)
    values (9, 'FT dumper', '2 Tonne Front Tip Dumper', 200);
insert into plant_inventory_entry (id, name, description, price)
    values (10, 'FT dumper', '2 Tonne Front Tip Dumper', 300);
insert into plant_inventory_entry (id, name, description, price)
    values (11, 'FT dumper', '3 Tonne Front Tip Dumper', 400);
insert into plant_inventory_entry (id, name, description, price)
    values (12, 'Loader', 'Hewden Backhoe Loader', 200);
insert into plant_inventory_entry (id, name, description, price)
    values (13, 'D-Truck', '15 Tonne Articulating Dump Truck', 250);
insert into plant_inventory_entry (id, name, description, price)
    values (14, 'D-Truck', '30 Tonne Articulating Dump Truck', 300);

insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (1, 1, 'A01', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (2, 2, 'A02', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (3, 3, 'A03', 'UNSERVICEABLEREPAIRABLE');

--findAvailablePlantsCount TEST
insert into plant_inventory_entry (id, name, description, price)
    values (1001, 'Bulldozer Tiny', '1 Tonne Bulldozer', 180);
insert into plant_inventory_entry (id, name, description, price)
    values (1002, 'Bulldozer Small', '2 Tonne Bulldozer', 200);
insert into plant_inventory_entry (id, name, description, price)
    values (1003, 'Bulldozer Large', '4 Tonne Bulldozer', 300);
insert into plant_inventory_entry (id, name, description, price)
    values (1004, 'Bulldozer Huge', '10 Tonne Bulldozer', 400);

insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (10001, 1001, 'BD011', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (10002, 1001, 'BD012', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (10003, 1001, 'BD013', 'UNSERVICABLECONDEMNED');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (10004, 1002, 'BD02', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (10005, 1003, 'BD03', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (10006, 1004, 'BD04', 'UNSERVICABLECONDEMNED');

---------------------
insert into plant_reservation (id, plant_id, start_date, end_date)
    values (1, 1, '2017-03-22', '2017-03-24');
insert into plant_reservation (id, plant_id, start_date, end_date)
    values (2, 2, '2017-01-25', '2017-03-26');
insert into plant_reservation (id, plant_id, start_date, end_date)
    values (3, 3, '2017-01-23', '2017-03-30');
insert into plant_reservation (id, plant_id, start_date, end_date)
    values (4, 10004, '2017-03-28', '2017-06-24');
insert into plant_reservation (id, plant_id, start_date, end_date)
    values (5, 3, '2017-04-28', '2017-09-24');
insert into plant_reservation (id, plant_id, start_date, end_date)
    values (6, 1, '2017-02-18', '2017-02-19');
insert into plant_reservation (id, plant_id, start_date, end_date)
    values (7, 1, '2017-02-25', '2017-02-26');
insert into plant_reservation (id, plant_id, start_date, end_date)
    values (8, 2, '2017-02-22', '2017-03-29');
insert into plant_reservation (id, plant_id, start_date, end_date)
    values (9, 3, '2017-01-25', '2017-03-26');

insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(10,'something','PREVENTIVE',10,'2017-03-22', '2017-03-24', 1);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(11,'something','PREVENTIVE',10,'2017-03-22', '2018-03-24', 1);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(12,'something','PREVENTIVE',10,'2017-03-22', '2019-03-24', 1);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(13,'something','PREVENTIVE',10,'2017-03-22', '2017-03-24', 1);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(14,'something','PREVENTIVE',10,'2017-03-22', '2016-03-24', 1);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(15,'something','PREVENTIVE',10,'2017-03-22', '2015-03-24', 1);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(16,'something','PREVENTIVE',10,'2017-03-22', '2014-03-24', 1);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(17,'something','PREVENTIVE',10,'2017-03-22', '2013-03-24', 1);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(24,'something','CORRECTIVE',10,'2017-03-22', '2015-03-24', 2);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(19,'something','CORRECTIVE',10,'2017-03-22', '2014-03-24', 2);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(20,'something','CORRECTIVE',10,'2017-03-22', '2013-03-24', 2);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(21,'something','CORRECTIVE',10,'2017-03-22', '2015-03-24', 2);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(22,'something','CORRECTIVE',10,'2017-03-22', '2014-03-24', 2);
insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(23,'something','CORRECTIVE',10,'2017-03-22', '2013-03-24', 5);


--Query the list of plants that have not been scheduled for any maintenance task during the last 12 months.

insert into plant_inventory_entry (id, name, description, price)
    values (15, 'D-Truck', '30 Tonne Articulating Dump Truck', 300);

insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition)
    values (4, 15, 'A01', 'SERVICEABLE');

insert into plant_reservation (id, plant_id, start_date, end_date)
    values (10, 4, '2018-03-22', '2018-03-24');

insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, plant_reservation_id)
    values(18,'Testing','PREVENTIVE',100,'2018-02-28', '2018-03-02', 2);

