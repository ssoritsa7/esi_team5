package com.example.esi_team5.models;

public enum TypeOfWork {
    PREVENTIVE, CORRECTIVE, OPERATIVE
}

