package com.example.esi_team5.models;

public enum POStatus {
    PENDING, REJECTED, OPEN, CLOSED
}