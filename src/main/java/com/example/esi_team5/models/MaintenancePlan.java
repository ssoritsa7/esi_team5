package com.example.esi_team5.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class MaintenancePlan {
    @Id
    @GeneratedValue
    Long id;
    Integer yearOfAction;
    @ManyToOne
    PlantInventoryItem plant;
    @ManyToMany
    List<MaintenanceTask> tasks;
}
