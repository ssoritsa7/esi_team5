package com.example.esi_team5.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data

public class PlantInventoryEntry {
    @Id
    @GeneratedValue
    Long id;
    //@OneToMany
    //List<PlantInventoryItem> stock;

    String name;
    String description;
    @Column(precision=8, scale=2)
    BigDecimal price;
}