package com.example.esi_team5.models;

public enum EquipmentCondition {
    SERVICEABLE, UNSERVICEABLEREPAIRABLE, UNSERVICABLEINCOMPLETE, UNSERVICABLECONDEMNED
}
