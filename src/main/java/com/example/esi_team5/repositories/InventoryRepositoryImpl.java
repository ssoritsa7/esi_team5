package com.example.esi_team5.repositories;

import com.example.esi_team5.models.PlantInventoryEntry;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

public class InventoryRepositoryImpl implements CustomInventoryRepository {

    @Autowired
    EntityManager em;

    public List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select p.plantInfo from PlantInventoryItem p where LOWER(p.plantInfo.name) like concat('%', ?1, '%') and p not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)",
                PlantInventoryEntry.class)
                .setParameter(1, name)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }

    public List<Object[]> findAvailablePlantsCount(String name, LocalDate startDate, LocalDate endDate) {
        LocalDate startDateWeekBefore = startDate.minusDays(7);
        LocalDate threeWeeksLater = LocalDate.now().plusDays(21);
        Boolean isInDistantFuture = threeWeeksLater.isBefore(startDate);

        Query query = em.createQuery(
                "select e, COUNT(i) " +
                        "from PlantInventoryEntry e " +
                        "join PlantInventoryItem i on i.plantInfo.id = e.id " +
                        "left join PlantReservation duplr on duplr.plant.id = i.id " +
                        "and (duplr.schedule.startDate between ?2 and ?3 OR duplr.schedule.endDate between ?2 and ?3) " +

                        "left join PlantReservation maintr on maintr.plant.id = i.id and maintr.schedule.endDate > ?4 " +
                        "left join MaintenanceTask mt on mt.plantReservation.id = maintr.id " +

                        "where LOWER(e.name) like ?1 " +
                        "and ((i.equipmentCondition = 'SERVICEABLE' AND duplr.id IS NULL) " +
                        "or (i.equipmentCondition != 'UNSERVICABLECONDEMNED' AND ?2 > ?5 " +
                        "and duplr.id IS NULL and mt.id IS NULL" +
                        ")" +
                        ") group by e.id", Object[].class )
                .setParameter(1, "%" + name + "%")
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .setParameter(4, startDateWeekBefore)
                .setParameter(5, threeWeeksLater);

        List<Object[]> results = query.getResultList();
        return results;
    }

    public List<Object[]> findAllPlantsNotScheduledForMaintenanceInTwelveMonths() {
        LocalDate date = LocalDate.now().minusMonths(12);

        return em.createQuery(
                "SELECT DISTINCT e " +
                        "FROM PlantInventoryEntry e " +
                        "JOIN PlantInventoryItem i on i.plantInfo.id = e.id " +
                        "LEFT JOIN PlantReservation r on r.plant.id = i.id " +
                        "LEFT JOIN MaintenanceTask m ON m.plantReservation.id = r.id " +
                        "WHERE m.businessPeriod.endDate < ?1", Object[].class)
                .setParameter(1, date)
                .getResultList();
    }

    public List<Object[]> findPreventiveRepairsByYears(int years){
        int currentYear = LocalDate.now().getYear()-years;
        Query query = em.createQuery("SELECT distinct YEAR(task.businessPeriod.endDate), sum(task.price)" +
                " FROM MaintenanceTask task WHERE task.typeOfWork = 'PREVENTIVE' and " +
                "YEAR(task.businessPeriod.endDate) >= ?1 and YEAR(task.businessPeriod.endDate) <= ?1+4" +
                        " group by YEAR(task.businessPeriod.endDate) order by YEAR(task.businessPeriod.endDate) desc",
                Object[].class).setParameter(1, currentYear);
        List<Object[]> result = query.getResultList();
        return result;
    }
    public List<Object[]> findPlantsInPeriod(LocalDate startDate, LocalDate endDate) {

        Query query = em.createQuery("SELECT i, COUNT(DISTINCT r) as rentals, COUNT(task) as tasks FROM PlantReservation r " +
                " join PlantInventoryItem  i on i.id = r.plant.id" +
                " left join MaintenanceTask task on (task.plantReservation.id = r.id) and (task.typeOfWork = 'CORRECTIVE')" +
                //" where task.typeOfWork = 'CORRECTIVE' " +
                " where (r.schedule.endDate >= ?1 and  r.schedule.endDate <= ?2 " +
                " or r.schedule.startDate >= ?1 and r.schedule.startDate <= ?2 " +
                " or r.schedule.startDate <= ?1 and r.schedule.endDate >= ?2)" +
                " group by i.id order by rentals desc, tasks desc", Object[].class)
                .setParameter(1, startDate)
                .setParameter(2, endDate);
        List<Object[]> results = query.getResultList();
        return results;
    }

}