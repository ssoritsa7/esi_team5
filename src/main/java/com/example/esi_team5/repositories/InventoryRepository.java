package com.example.esi_team5.repositories;

import com.example.esi_team5.models.PlantInventoryEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface InventoryRepository extends JpaRepository<PlantInventoryEntry, Long>, CustomInventoryRepository {
    List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate);
    List<Object[]> findAvailablePlantsCount(String name, LocalDate startDate, LocalDate endDate);
    List<Object[]> findAllPlantsNotScheduledForMaintenanceInTwelveMonths();
    List<Object[]> findPreventiveRepairsByYears(int years);
    List<Object[]> findPlantsInPeriod(LocalDate startDate, LocalDate endDate);
}
