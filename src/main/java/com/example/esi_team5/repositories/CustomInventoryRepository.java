package com.example.esi_team5.repositories;

import com.example.esi_team5.models.PlantInventoryEntry;

import java.time.LocalDate;
import java.util.List;

public interface CustomInventoryRepository {
    List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate);

    List<Object[]> findAvailablePlantsCount(String name, LocalDate startDate, LocalDate endDate);
    List<Object[]> findAllPlantsNotScheduledForMaintenanceInTwelveMonths();
}
