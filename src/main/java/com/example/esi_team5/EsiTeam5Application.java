package com.example.esi_team5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsiTeam5Application {

    public static void main(String[] args) {
        SpringApplication.run(EsiTeam5Application.class, args);
    }

}
